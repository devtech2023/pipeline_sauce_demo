import time
import unittest

from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

class TestFullPage(unittest.TestCase):
    
    browser = Firefox()

    def test_valid_full(self):
        
        # login credencials validation user
        self.browser.get('https://www.saucedemo.com/')
        
        campo_login_validation = self.browser.find_element(By.ID, 'user-name')
        campo_login_validation.send_keys('user_test_credentials')
        campo_password_validation = self.browser.find_element(By.ID, 'password')
        campo_password_validation.send_keys('secret')
        botao_login_validation = self.browser.find_element(By.ID, 'login-button')
        time.sleep(4)
        
        botao_login_validation.click() 
        
        campo_login_validation.clear()
        campo_password_validation.clear()

        time.sleep(4)
        
        campo_login = self.browser.find_element(By.ID, 'user-name')
        campo_login.send_keys('standard_user')
        campo_password = self.browser.find_element(By.ID, 'password')
        campo_password.send_keys('secret_sauce')
        botao_login = self.browser.find_element(By.ID, 'login-button')
        time.sleep(4)
        
        botao_login.click() 
        
        time.sleep(3)
        
        # open more tabs
        for _ in range(4):
            self.browser.execute_script("window.open('', '_blank');")
        
        for i in range(1, 5):
            nova_aba = self.browser.window_handles[i]
            self.browser.switch_to.window(nova_aba)
            self.browser.get(f'https://www.saucedemo.com/inventory.html')
            
        self.browser.switch_to.window(self.browser.window_handles[0])

        time.sleep(4)

        # choose products
        label_titulo = self.browser.find_element(By.CLASS_NAME, 'title').text
        self.assertEqual('Products', label_titulo)
        time.sleep(4)
        campo_select_product = self.browser.find_element(By.CLASS_NAME, 'inventory_item_name')
        campo_select_product.click()
        time.sleep(4)
        campo_add_cart = self.browser.find_element(By.NAME, 'add-to-cart-sauce-labs-backpack')
        campo_add_cart.click()
        time.sleep(4)        
        campo_go_cart = self.browser.find_element(By.CLASS_NAME, 'shopping_cart_link')
        campo_go_cart.click()
        time.sleep(4) 
        campo_checkout = self.browser.find_element(By.NAME, 'checkout')
        campo_checkout.click()
        
        time.sleep(4)
        
        # checkout products
        campo_check_name = self.browser.find_element(By.ID, 'first-name')
        campo_check_name.send_keys('first_name_user')
        campo_check_last_name = self.browser.find_element(By.ID, 'last-name')
        campo_check_last_name.send_keys('last_name_user')
        campo_check_zip_code = self.browser.find_element(By.ID, 'postal-code')
        campo_check_zip_code.send_keys('69029000')
        time.sleep(4)
        botao_checkout = self.browser.find_element(By.ID, 'continue')
        botao_checkout.click()
        time.sleep(4)
        botao_finish = self.browser.find_element(By.ID, 'finish')
        botao_finish.click()
        time.sleep(4)
        botao_home = self.browser.find_element(By.ID, 'back-to-products')
        botao_home.click()
        
        time.sleep(4)
        
        # test icons social midia
        click_twitter = self.browser.find_element(By.CLASS_NAME, 'social_twitter')
        click_twitter.click()
        click_facebook = self.browser.find_element(By.CLASS_NAME, 'social_facebook')
        click_facebook.click()
        click_linkedin = self.browser.find_element(By.CLASS_NAME, 'social_linkedin')
        click_linkedin.click()
        
        self.browser.switch_to.window(self.browser.window_handles[0])

        time.sleep(4)
        
        # logout credencials user
        botao_menu = self.browser.find_element(By.ID, 'react-burger-menu-btn')
        botao_menu.click()
        time.sleep(4)
        botao_logout = self.browser.find_element(By.ID, 'logout_sidebar_link')
        botao_logout.click()
        
if __name__ == '__main__':
    unittest.main()     